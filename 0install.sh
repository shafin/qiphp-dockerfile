#!/bin/bash

#assume that you have already created the ssh into bitbucket
echo "this is to initialize the digital Ocean image"
#ubuntu commons
sudo apt-get update &&
sudo apt-get install -y software-properties-common python-software-properties build-essential curl tree automake rlwrap gnome-tweak-tool httpie &&

#git
sudo apt-get install -y git git-gui &&

#java
sudo add-apt-repository -y ppa:webupd8team/java &&
sudo apt-get update &&

echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections &&
sudo apt-get -y install oracle-java8-installer &&
sudo apt-get -y install oracle-java8-set-default &&

#ant
sudo apt-get install -y ant &&

#maven
sudo apt-get install -y maven &&

#gradle
sudo add-apt-repository ppa:cwchien/gradle -y &&
sudo apt-get update &&
sudo apt-get install -y gradle &&

# Nodjs and npm
sudo add-apt-repository ppa:chris-lea/node.js &&
sudo apt-get update &&
sudo apt-get install nodejs &&
sudo npm install -g npm &&

mkdir ~/npm &&
echo 'export PATH=~/npm/bin:$PATH' >> ~/.bashrc && source ~/.bashrc &&
echo 'export NODE_PATH=~/npm/lib/node_modules:$NODE_PATH' >> ~/.bashrc && source ~/.bashrc &&
npm config set prefix ~/npm &&



#Install nodejs packages
npm install -g bower &&
npm install -g gulp &&

#If the installation fails, try cleaning the cache using the following two commands:
#npm cache clean
#ower cache clean

#If you are “getting error parsing json”, try specifying the registry name:
#npm install -g yo --registry http://registry.npmjs.org/

#If you are getting permission related errors, run the following commands:
#sudo rm -rf ~/.npm
#sudo rm -rf ~/npm

#Install PHP
#php
sudo apt-get update &&
sudo apt-get install python-software-properties &&
sudo add-apt-repository ppa:ondrej/php5-5.6 &&
sudo apt-get install php5 &&
sudo apt-get install php5-mcrypt &&
sudo apt-get install php5-xdebug &&
sudo apt-get install php5-pgsql &&
sudo apt-get install php5-sqlite &&

#composer
curl -sS https://getcomposer.org/installer | php &&
sudo mv composer.phar /usr/local/bin/composer &&
composer selfupdate &&

#laravel
composer global require "laravel/installer=~1.1" &&
export PATH="~/.composer/vendor/bin/:$PATH" &&

## Apache Server
sudo php5enmod mcrypt &&
sudo a2enmod php5 &&
sudo service apache2 restart &&



# Clone niopack
mkdir ~/QI &&
mkdir ~/QI/Source &&
cd ~/QI/Source &&
git clone git@bitbucket.org:qianalysisinc/niopack.git &&
cd niopack &&
./refresh.sh &&
cd .. &&

#Clone qijava
git clone git@bitbucket.org:qianalysisinc/qijava.git &&
cd .. &&

# Clone qiphp
git clone git@bitbucket.org:qianalysisinc/qiphp.git &&
cd qiphp &&
# take out the last three lines bower install, gulp, and php artisan serve
./refresh.sh  &&


#fish - optional to use upgraded fish
sudo apt-get remove fish &&
sudo apt-add-repository ppa:fish-shell/release-2 &&
sudo apt-get update &&
sudo apt-get install fish &&


sudo apt-get remove apache2 
# removing apache2 makes port 80 available (this is just optional)

