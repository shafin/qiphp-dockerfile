#!/bin/bash

docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker rmi $(docker images -q --filter "dangling=true")

cd ~/qiphp-dockerfile/Example/ubuntu/
docker build -t thetigerworks/ubuntu:14.04 .

cd ~/qiphp-dockerfile/Example/laravel/
docker build -t thetigerworks/thetigerworks:latest .
