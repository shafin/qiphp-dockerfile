#!/bin/bash

docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker rmi $(docker images -q --filter "dangling=true")

cd ~/qiphp-dockerfile/ubuntu
docker build -t qianalysis/ubuntu:14.04 .

cd ~/qiphp-dockerfile/
docker build -t qianalysis/qiphp:latest .
