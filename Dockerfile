FROM qianalysis/ubuntu:14.04
# Install Nginx.
RUN \
  apt-get update && \
  apt-get install -y software-properties-common && \
  add-apt-repository -y ppa:nginx/stable && \
  apt-get update && \
  apt-get install -y nginx wget curl nano

# Install HHVM
RUN \
  wget -O - http://dl.hhvm.com/conf/hhvm.gpg.key | sudo apt-key add - && \
  echo deb http://dl.hhvm.com/ubuntu trusty main | sudo tee /etc/apt/sources.list.d/hhvm.list && \
  apt-get update && \
  apt-get install -y curl hhvm && \
  service nginx stop

# Nginx bridge to HHVM over FastCGI.
RUN \
  /usr/share/hhvm/install_fastcgi.sh && \
  /usr/bin/update-alternatives --install /usr/bin/php php /usr/bin/hhvm 60

# Add vhost
RUN rm /etc/nginx/sites-available/*
RUN rm /etc/nginx/sites-enabled/*
ADD default /etc/nginx/sites-available/default
ADD default /etc/nginx/sites-enabled/default
RUN rm /var/www/html/*

# clone project from git
ADD qiphp /var/www/html/
RUN chown -R www-data:www-data /var/www

#TO FIX COMPOSER ERROR
RUN locale-gen en_US.UTF-8
RUN dpkg-reconfigure locales
RUN service nginx restart
RUN service hhvm restart
#FIX ENDS

#-----------------------------------------------------------------------------------------------------------------------------------------------#

# Nodjs and npm
RUN add-apt-repository ppa:chris-lea/node.js
RUN apt-get update
RUN apt-get install -y nodejs
RUN npm install -g npm
RUN printf '\n# Node.js\nexport PATH="node_modules/.bin:$PATH"' >> /root/.bashrc

#RUN mkdir /root/npm
#RUN echo 'export PATH=/root/npm/bin:$PATH' >> /root/.bashrc && source /root/.bashrc
#RUN echo 'export NODE_PATH=/root/npm/lib/node_modules:$NODE_PATH' >> /root/.bashrc && source /root/.bashrc
#RUN npm config set prefix /root/npm

#Install nodejs packages
RUN npm install -g bower
RUN npm install -g gulp

#php
RUN apt-get update
RUN apt-get install -y python-software-properties
RUN add-apt-repository ppa:ondrej/php5-5.6
RUN apt-get install -y php5
RUN apt-get install -y php5-mcrypt
RUN apt-get install -y php5-xdebug
RUN apt-get install -y php5-pgsql
RUN apt-get install -y php5-sqlite

#install composer
RUN /usr/bin/curl -sS https://getcomposer.org/installer |/usr/bin/php
RUN /bin/mv composer.phar /usr/local/bin/composer

#laravel
RUN apt-get install -y git
RUN apt-get -yq install ssh
RUN composer global require "laravel/installer=~1.1"
RUN export PATH="~/.composer/vendor/bin/:$PATH"

#-----------------------------------------------------------------------------------------------------------------------------------------------#


WORKDIR /var/www/html
RUN npm install bower -g
RUN npm update
RUN composer install
RUN bower install --allow-root
RUN gulp

# Clean up
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


# Define working directory.
WORKDIR /etc/nginx
# Expose ports.
EXPOSE 80
# Append "daemon off;" to the beginning of the configuration
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
# ADD that script.
ADD start.sh /etc/nginx/start.sh
# Set permission for startup script
RUN chmod u+x /etc/nginx/start.sh
RUN mkdir -p /etc/service/start/
ADD start.sh /etc/service/start/run
RUN chmod a+x /etc/service/start/run
CMD ["/usr/sbin/runsvdir-start"]
RUN hhvm /var/www/html/public/index.php
